<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation">
  <documentation>
    <title>Local Python Function</title>
    <authors>
      <author>Fabian Wein</author>
    </authors>
    <date>2022-03-02</date>
    <description>Use in optimization a local function and its gradient from a python implementation.
       Here we reimplement the slope constraints.
       Provide sparsity, eval and grad. The first contains also the number of local functions.
       eval and grad are called for each local function.
       See the python_function test case for further description.
    </description>
  </documentation>

  <!-- In the given script the functions called from optimization are implemented. -->
  <python file="local_kernel.py" path="." />
  
  <fileFormats>
    <output>
     <hdf5 />
     <info/>
    </output>
    <materialData file="mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region material="99lines" name="mech" />
    </regionList>
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <mechanic subType="planeStress">
        <regionList>
          <region name="mech" />
        </regionList>

        <bcsAndLoads>
           <fix name="left"> 
              <comp dof="x"/> 
              <comp dof="y"/> 
           </fix>
           <force name="bottom_right" >
             <comp dof="y" value="-1" />
           </force>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
          <elemResult type="mechPseudoDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="physicalPseudoDensity">
            <allRegions/>
          </elemResult>
          <regionResult type="mechTotalEnergy">
            <allRegions/>
          </regionResult>

        </storeResults>
      </mechanic>
    </pdeList>

  </sequenceStep>

    
  <optimization>
    <costFunction type="compliance" task="minimize" />

    <!-- we could also use concurrently the python implementation of the python_function test -->      
    <constraint type="volume" value=".5" bound="upperBound"/>

    <!-- This is a local python implementation of the slope constraints. We do not make use of slopes bounded
    feature but implement | x_i - x_i+1 | as (x_i - x_i+1) and (-x_i + x_i+1). All for right and upper neighbor.
    sparsity is mandatory and gives a list on numpy arrays of on. The list size determines the number of local
    functions and the 0-based index is given to the eval and grad function. Note that the argument for global
    python functions is a dictionary of options.  -->
    <constraint type="localPython" value=".3" bound="upperBound" linear="true" mode="constraint"  >
      <python name="slope" sparsity="slope_sparsity" eval="slope_eval" grad="slope_grad" script="kernel" />  
    </constraint>

    <optimizer type="snopt" maxIterations="5">
      <snopt>
        <option name="verify_level" type="integer" value="-1"/>
      </snopt>
    </optimizer>

    <ersatzMaterial region="mech" material="mechanic" method="simp" >
      <design name="density" initial=".5" physical_lower="1e-9" upper="1.0" />
      <transferFunction type="simp" application="mech" param="3"/>
      <export save="last" write="iteration" />
    </ersatzMaterial>
 
    <commit mode="forward" stride="1"/>
  </optimization>
</cfsSimulation>

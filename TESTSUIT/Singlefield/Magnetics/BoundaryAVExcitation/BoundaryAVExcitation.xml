<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns="http://www.cfs++.org/simulation">
    <documentation>
        <title>Voltage excitation of a straight solid cylindrical inductor in A-V,A formulation</title>
        <authors>
            <author>kroppert</author>
        </authors>
        <date>2019-01-06</date>
        <keywords>
            <keyword>magneticEdge</keyword>
        </keywords>
        <references> </references>
        <isVerified>yes</isVerified>
        <description> 
            We introduce the concept of voltage through the prescription of
            an electric scalar potential at the electric ports of the
            inductor, touching the boundary of the computational domain.
            
            We then compare the resulting current in the inductor (integrated
            eddy currents over the electric port surface) to an analytical
            model, where we compute the impedance of the solid conductor.
            
            The comparison between CFS++ and the analytic model is
            performed in the evel.ipynb notebook.
        </description>
    </documentation>
    <!-- define which files are needed for simulation input & output-->
    <fileFormats>
        <input>
            <hdf5 fileName="meshCylAir.cfs"/>
        </input>
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <!-- specify your material file here -->
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>
    
    <!-- material assignment -->
    <domain geometryType="3d">
        <regionList>
            <region name="V_air" material="air"/>
            <region name="V_coil" material="copper"/>
            <!--air copper iron-->
        </regionList>
        <surfRegionList>
            <surfRegion name="S_air_out"/>
            <surfRegion name="S_z_pos"/>
            <surfRegion name="S_z_neg"/>
            <surfRegion name="S_sigma_plus"/>
            <surfRegion name="S_sigma_minus"/>
        </surfRegionList>
    </domain>

    <fePolynomialList>
        <Legendre id="Hcurl">
            <isoOrder>0</isoOrder>
        </Legendre>
        <Lagrange id="H1">
            <isoOrder>1</isoOrder>
        </Lagrange>
    </fePolynomialList>


    <sequenceStep index="1">
        <analysis>
            <harmonic>
                <numFreq>20</numFreq>
                <startFreq>1</startFreq>
                <stopFreq>5000</stopFreq>
            </harmonic>
        </analysis>

        <pdeList>
            <magneticEdge formulation="A-V">
                <regionList>
                    <region name="V_coil" magVecPolyId="Hcurl" elecScalPolyId="H1" isConducRegion="true"/>
                    <region name="V_air" magVecPolyId="Hcurl" elecScalPolyId="H1" isConducRegion="false"/>
                </regionList>

                <bcsAndLoads>
                    <fluxParallel name="S_air_out"/>
                    <fluxParallel name="S_z_pos"/>
                    <fluxParallel name="S_z_neg"/>
                    <fluxParallel name="S_sigma_plus"/>
                    <fluxParallel name="S_sigma_minus"/>
                    
                    <elecPotential name="S_sigma_plus" value="1"/> 
                    
                    <elecPotential name="S_sigma_minus" value="0"/>
                </bcsAndLoads>

                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential">
                        <allRegions/>
                    </elemResult>
                    <nodeResult type="elecPotential">
                        <regionList>
                            <region name="V_coil"/>
                        </regionList>
                    </nodeResult>
                    <elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="gradElecPotential">
                        <regionList>
                            <region name="V_coil"/>
                        </regionList>
                    </elemResult>
                    <elemResult type="magPotentialD1">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magEddyCurrentDensity">
                        <regionList>
                            <region name="V_coil"/>
                        </regionList>
                    </elemResult>
                    <elemResult type="gradElecPotential">
                        <regionList>
                            <region name="V_coil"/>
                        </regionList>
                    </elemResult>
                    <surfRegionResult type="magEddyCurrent1" complexFormat="realImag">
                        <surfRegionList>
                            <surfRegion name="S_sigma_plus" neighborRegion="V_coil"/>
                        </surfRegionList>
                    </surfRegionResult>
                    <surfRegionResult type="magEddyCurrent2" >
                        <surfRegionList>
                            <surfRegion name="S_sigma_plus" neighborRegion="V_coil"/>
                        </surfRegionList>
                    </surfRegionResult>
                </storeResults>
            </magneticEdge>
        </pdeList>


        <linearSystems>
            <system>
                <solutionStrategy>
                    <standard>
                        <matrix reordering="Metis"/>
                    </standard>
                </solutionStrategy>
                <solverList>
                    <pardiso/>
                </solverList>
            </system>
        </linearSystems>
    </sequenceStep>
</cfsSimulation>

<?xml version="1.0"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  
  <documentation>
    <title>3D cylindrical coil with hysteretic core material</title>
    <authors>
      <author>Michael Nierla</author>
    </authors>
    <date>2019-07-09</date>
    <keywords>
      <keyword>magneticEdge</keyword>
      <keyword>hysteresis</keyword>
      <keyword>linesearch</keyword>
      <keyword>transient</keyword>
    </keywords>
    <references>none</references>
    <isVerified>no</isVerified>
    <description>
      Geometry:
	  3D-eigth model of a cylindrical copper coil with iron/FeCo core
	  surrounded by air; same setup as in Magnetic testcases Coil3DEdge***
	  
      Sketch of x-z-crosssection:
      
      z-axis
      |
      | air
      |________ _
	  |        | | 
 	  |  core  |coil 
  	  |        | | 
 	  |        | | 
	  |        | | 
	  |________|_|_________ x-axis
      
	  Idea behind testcase:
	  - test hysteresis for 3D magneticEdge formulation 
	  - test vector hysteresis model based on rotational operators in 3D
	  - test a quasi-newton scheme in 3D
	
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <hdf5 fileName="CylindricCoil.h5"/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="3d" printGridInfo="yes">
    <variableList>
      <var name="depth" value="50e-3*2"/>
      <var name="radius" value="5e-3"/>
    </variableList>
    
    <!-- now we have both jumps in conductivity and permeability 
	this case is known to be problematic for nodal elements -->
    <regionList>
      <region name="air"  material="Air"/>
      <region name="coil" material="Copper"/>
      <region name="core" material="FECO_Sutor_Revised"/>
    </regionList>
    
    <!-- Cylindric coordinate system for coil current -->
    <coordSysList>
      <cylindric id="mid"> 
        <origin x="0" y="0" z="0"/>
        <zAxis z="1" />
        <rAxis x="1" />
      </cylindric>
    </coordSysList>
  </domain>
  
  <fePolynomialList>
    <Lagrange>
      <isoOrder>1</isoOrder>
    </Lagrange>
  </fePolynomialList>
  
  <!-- ================================= -->
  <!--  E D G E   F O R M U L A T I O N  -->
  <!-- ================================= -->
  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>3</numSteps>
        <deltaT>5.55555e-3</deltaT>
      </transient>
    </analysis>
    <pdeList>
      <magneticEdge systemId="default">
        <regionList>
          <region name="air"/>
          <region name="coil"/>
          <region name="core" nonLinIds="p"/>
        </regionList>

		<nonLinList>
			<hysteresis id="p"/>
        </nonLinList>

		<bcsAndLoads>
          <fluxParallel name="x-inner"/>
          <fluxParallel name="x-outer"/>
          <fluxParallel name="y-inner"/>
          <fluxParallel name="z-outer"/>
        </bcsAndLoads>
        
        <coilList>
          <coil id="myCoil">
            <source type="current" value="4500*sample1D('inputCurrent2.txt',t,1)"/>
              <part>
                <regionList>
                  <region name="coil"/>
                </regionList>
                <direction>
                  <analytic coordSysId="mid">
                    <comp dof="phi" value="1"/>
                  </analytic>
                </direction>
                <wireCrossSection area="1e-6"/>
                <resistance value="0"/> 
              </part>
            </coil>
          </coilList>
        
        <storeResults>
          <elemResult type="magPotential">
            <allRegions/>
          </elemResult>

          <elemResult type="magFluxDensity">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
		 
		        <elemResult type="magFieldIntensity">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
		        </elemResult>
          
          <elemResult type="magPolarization">
            <allRegions/>
            <elemList>
              <elems name="center" outputIds="txt"/>
            </elemList>
          </elemResult>
          	 
	  <elemResult type="magEddyCurrentDensity">
	      <allRegions/>
	      <elemList>
	      <elems name="hist" outputIds="txt"/>
	      </elemList>
	   </elemResult>
		  
          <regionResult type="magEnergy">
            <allRegions outputIds="txt"/>
          </regionResult>
         
        </storeResults>
        
      </magneticEdge>
    </pdeList>
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <!-- setup solution process for non-linear, hysteretic system -->
            <!-- minLoggingToTerminal = 0 > disable direct output of non-linear iterations;
              = 1 > write incremental, residual error, linesearch factor etc. to cout
              after each iteration	
              = 2 > as 1 but write out overview over all iterations at end of a timestep -->
            <hysteretic loggingToFile="yes" loggingToTerminal="2">
              <solutionMethod>
                <QuasiNewton_ChordMethod initialNumberFPSteps="2" calculateFDJacobianAtMidpointOnly="no"/>
              </solutionMethod>
              <lineSearch selectionCriterionForMultipleLS="1">
                <None></None>
              </lineSearch>
              <stoppingCriteria>
                <increment_relative value="1E-5" checkingFrequency="1" firstCheck="1" scaleToSystemSize="no"/>
                <residual_relative value="3E-5" checkingFrequency="1" firstCheck="1" scaleToSystemSize="no"/>
              </stoppingCriteria>
              <evaluationDepth>
                <evaluateAtIntegrationPoints_OneOperatorPerElement></evaluateAtIntegrationPoints_OneOperatorPerElement>
              </evaluationDepth>
              <failbackCriteria>
                <residual_relative value="1E-5" firstCheck="10" checkingFrequency="2" scaleToSystemSize="no"/>
              </failbackCriteria>
              <maxIter>50</maxIter>
            </hysteretic>
          </standard>
        </solutionStrategy>
      </system>
    </linearSystems>
   </sequenceStep>
   
  </cfsSimulation>

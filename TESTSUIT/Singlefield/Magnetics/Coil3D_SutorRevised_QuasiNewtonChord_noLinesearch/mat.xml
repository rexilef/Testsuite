<?xml version='1.0' encoding='utf-8'?>
<cfsMaterialDataBase xmlns="http://www.cfs++.org/material">
  <!-- STANDARD LINEAR MATERIALS -->
  <material name="Air">
    <magnetic>
      <electricConductivity>
        <linear>
          <isotropic>
            <real> 0.0
          </real>
          </isotropic>
        </linear>
      </electricConductivity>
      <permeability>
        <linear>
          <isotropic>
            <real> 1.25664e-06 </real>
          </isotropic>
        </linear>
      </permeability>
    </magnetic>
  </material>
  <material name="Epcos_N87">
    <magnetic>
      <electricConductivity>
        <linear>
          <isotropic>
            <real> 0.1 </real>
          </isotropic>
        </linear>
      </electricConductivity>
      <permeability>
        <linear>
          <isotropic>
            <real> 2.7646e-3 </real>
          </isotropic>
        </linear>
      </permeability>
    </magnetic>
  </material>
  <material name="NdFeB">
    <mechanical>
      <density>
        <linear>
          <real>  7500 </real>
        </linear>
      </density>
      <elasticity>
        <linear>
          <isotropic>
            <elasticityModulus>
              <real> 1.6E+11 </real>
            </elasticityModulus>
            <poissonNumber>
              <real> 0.24 </real>
            </poissonNumber>
          </isotropic>
        </linear>
      </elasticity>
    </mechanical>
    <magnetic>
      <electricConductivity>
        <linear>
          <isotropic>
            <real>6.67E5</real>
          </isotropic>
        </linear>
      </electricConductivity>
      <permeability>
        <linear>
          <isotropic>
            <real>  1.3188E-6 </real>
          </isotropic>
        </linear>
      </permeability>
    </magnetic>
  </material>
  <material name="Copper">
    <magnetic>
      <electricConductivity>
        <linear>
          <isotropic>
            <real>5.96e+07</real>
          </isotropic>
        </linear>
      </electricConductivity>
      <permeability>
        <linear>
          <isotropic>
            <real>  1.25663e-06 </real>
          </isotropic>
        </linear>
      </permeability>
    </magnetic>
  </material>
  <material name="Iron">
    <magnetic>
      <electricConductivity>
        <linear>
          <isotropic>
            <real>1.0E+5</real>
          </isotropic>
        </linear>
      </electricConductivity>
      <permeability>
        <linear>
          <isotropic>
            <real> 9.17290E-04 </real>
          </isotropic>
        </linear>
        <nonlinear>
          <isotropic>
            <dependency></dependency>
            <approxType>smoothSplines</approxType>
            <measAccuracy>   0.05      </measAccuracy>
            <maxApproxVal>   3.0       </maxApproxVal>
            <dataName>bh1.fnc</dataName>
          </isotropic>
        </nonlinear>
      </permeability>
    </magnetic>
  </material>
  <!-- Materials with different Preisach hysteresis models; documentation mainly in FECO_Mayergoyz_3;
  further documentation available in Oxygen from tool-tip help -->
  <material name="FECO_Mayergoyz_3">
    <magnetic>
      <electricConductivity>
        <linear>
          <isotropic>
            <real> 1.35e7 </real>
          </isotropic>
        </linear>
      </electricConductivity>
      <permeability>
        <linear>
          <isotropic>
            <real> 1.25663e-6 </real>
          </isotropic>
        </linear>
      </permeability>
      <hystModel>
        <magPolarization>
          <vectorPreisach_Mayergoyz>
            <isotropic>
              <numDirections>3</numDirections>
              <ScalarModel>
			  <!-- scalar model measured via VVSM at LSE for round sputtered FECO-disc of 6 \mu m thickness on messing target -->
                <inputSat> 118807.897754412654 </inputSat>
                <outputSat> 1.776287668937 </outputSat>
                <weights>
                  <dim_weights>200</dim_weights>
                  <weightType>
					<!-- muDat and anhyst parameter were determined via LeastSquares Fitting to measured major loop -->
                    <muDat>
                      <A> 2.654743006651 </A>
                      <h2> 0.089052884292 </h2>
                      <sigma2> 5.189442209595 </sigma2>
                      <eta> 1.620347107219 </eta>
					  <!-- fitting script (originally developed by M. Loeffler for his Dipl. Thesis) determines the
					  muDat parameter for a normalized curve; the curve is thereby normalized to the range -0.5 to 
					  0.5 times saturation; cfs uses internally a normalization to -1 to +1 range; by setting the 
					  forHalfRange parameter to true will trigger a rescaling in cfs -->
                      <forHalfRange>true</forHalfRange>
                    </muDat>
                  </weightType>
                  <anhystereticParameter>
                    <a> 0.347647727062 </a>
                    <b> 4.749596097652 </b>
                    <c>0</c>
                    <d>0.0E1</d>
                    <onlyAnhyst>false</onlyAnhyst>
                    <forHalfRange>true</forHalfRange>
					<!-- if the following parameter is set to false, the preisach model itself will reach outputSat at inputSat;
					the additional anhysteretic part will such lead to a total output which is larger than outputSat; if the parameter
					is set to true instead, the output will be rescaled such that preisach model + anhysteretic part will together reach
					outputSat for an input of amplitude inputSat -->
					<anhystPartCountsTowardsOutputSat>true</anhystPartCountsTowardsOutputSat>
					</anhystereticParameter>
					</weights>
			<!-- although the MayergoyzVectorModel consists of numDirections (see above) scalar models, the weighting function for the
				single scalar models cannot be taken directly; instead the weights have to be transferred to the vector case first; cfs has
				the required functions for 2D and 3D isotropic materials; by setting belows flag to false, this transfer-function is used -->
				<weightsAdaptedToMayergoyzVectorModel>false</weightsAdaptedToMayergoyzVectorModel>
				</ScalarModel>
				</isotropic><!-- in magnetics, we usually solve for B in the FE-system but to evaluate the Preisach hysteresis model, we require H as input;
			therefore, we have to apply an inversion on local level (except for fixpointH method), i.e., on each element/integration point we 
			have to solve B = mu H + Preisach(H) -->
			<hystInversion>
              <InversionMethod>
				<!-- currently, multiple local inversion methods are available; however, only NewtonWithLinesearch and Fixpoint work properly
				for vector models -->
				<NewtonWithLinesearch>
				  <!-- the used linsearch is based on a simple backtracking approach (steplength will be decreased by factor 2 in each iteration;
				  eta with smallest residual will be taken); if stopLinesearchAtLocalMin is true, iteration will stop if residual increased twice in
				  a row -->
				  <stopLinesearchAtLocalMin> true </stopLinesearchAtLocalMin>
				  <!-- for Newton, we have to compute the Jacobian of the residual; currently only forwardBackwardDifferences work properly -->
				  <jacobiImplementation>
					<ForwardBackwardDifferences/>
				   </jacobiImplementation>
				  <alphaLSMax>1.0E0</alphaLSMax>
				  <alphaLSMin>1.0E-3</alphaLSMin>
				  <!-- the resolution for the Jacobian should not be smaller than sqrt(machine-precision) -->
				  <jacobiResolution>1.0E-7</jacobiResolution>
				  <numberOfLinesearchSteps>50</numberOfLinesearchSteps>
				  </NewtonWithLinesearch></InversionMethod>
				  <maxNumberOuterIterations>25</maxNumberOuterIterations>
				 <!-- stopping criteria for Inversion; residual wrt. H = H - nu_0(B - Polarization)
			  residual wrt. B = B - mu_0(H + Magnetization); residual wrt B much easier to satisfy as
			  general values are smaller; therefore residual wrt. H is the main criterion, residual wrt. B
			  is only used as failback criterion -->
			  <residualTolH value="1e-6" isRelative="no"/>
              <residualTolB value="1e-9" isRelative="no"/>
            </hystInversion>
            <clipOutputToSat>
			  <!-- due to the summation of multiple scalar models, the Mayergoyz vector model actually grows in output beyond the saturation
			  value if input goes over saturation; we can surpress this by clipping -->
			  <noClipping/>
			</clipOutputToSat>
		</vectorPreisach_Mayergoyz>
		</magPolarization>
		</hystModel>
    </magnetic>
  </material>
  <!-- for documentation see FECO_Mayergoyz_3 -->
  <material name="FECO_Mayergoyz_5">
    <magnetic>
      <electricConductivity>
        <linear>
          <isotropic>
            <real> 1.35e7 </real>
          </isotropic>
        </linear>
      </electricConductivity>
      <permeability>
        <linear>
          <isotropic>
            <real> 1.25663e-6 </real>
          </isotropic>
        </linear>
      </permeability>
      <hystModel>
        <magPolarization>
          <vectorPreisach_Mayergoyz>
            <isotropic>
              <numDirections>5</numDirections><ScalarModel>
                <inputSat> 118807.897754412654 </inputSat>
				<outputSat> 1.776287668937 </outputSat>
				<weights>
                  <dim_weights>200</dim_weights><weightType>
                    <muDat>
                      <A> 2.654743006651 </A>
                      <h2> 0.089052884292 </h2>
                      <sigma2> 5.189442209595 </sigma2>
                      <eta> 1.620347107219 </eta>
                      <forHalfRange>true</forHalfRange>
                    </muDat>
                  </weightType>
                  <anhystereticParameter>
                    <a> 0.347647727062 </a>
                    <b> 4.749596097652 </b>
                    <c>0</c>
                    <d>0.0E1</d>
                    <onlyAnhyst>false</onlyAnhyst>
                    <forHalfRange>true</forHalfRange>
                    <anhystPartCountsTowardsOutputSat>true</anhystPartCountsTowardsOutputSat>
                  </anhystereticParameter>
                </weights>
                <weightsAdaptedToMayergoyzVectorModel>false</weightsAdaptedToMayergoyzVectorModel>
              </ScalarModel>
            </isotropic>
            <hystInversion>
              <InversionMethod> 
                <NewtonWithLinesearch>
                  <stopLinesearchAtLocalMin> true </stopLinesearchAtLocalMin>
                  <jacobiImplementation>
                    <ForwardBackwardDifferences></ForwardBackwardDifferences>
                  </jacobiImplementation>
                  <alphaLSMax>1.0E0</alphaLSMax>
                  <alphaLSMin>1.0E-3</alphaLSMin>
                  <jacobiResolution>1.0E-7</jacobiResolution>
                  <numberOfLinesearchSteps>50</numberOfLinesearchSteps>
                </NewtonWithLinesearch>
              </InversionMethod>
              <maxNumberOuterIterations>25</maxNumberOuterIterations>
			  <residualTolH value="1e-6" isRelative="no"/>
              <residualTolB value="1e-9" isRelative="no"/>
            </hystInversion>
            <clipOutputToSat>
              <noClipping></noClipping>
            </clipOutputToSat>
          </vectorPreisach_Mayergoyz>
        </magPolarization>
      </hystModel>
    </magnetic>
  </material>
  <material name="FECO_Mayergoyz_9">
    <magnetic>
      <electricConductivity>
        <linear>
          <isotropic>
            <real> 1.35e7 </real>
          </isotropic>
        </linear>
      </electricConductivity>
      <permeability>
        <linear>
          <isotropic>
            <real> 1.25663e-6 </real>
          </isotropic>
        </linear>
      </permeability>
      <hystModel>
        <magPolarization>
          <vectorPreisach_Mayergoyz>
            <isotropic>
              <numDirections>9</numDirections><ScalarModel>
                <inputSat> 118807.897754412654 </inputSat>
				<outputSat> 1.776287668937 </outputSat>
				<weights>
                  <dim_weights>200</dim_weights><weightType>
                    <muDat>
                      <A> 2.654743006651 </A>
                      <h2> 0.089052884292 </h2>
                      <sigma2> 5.189442209595 </sigma2>
                      <eta> 1.620347107219 </eta>
                      <forHalfRange>true</forHalfRange>
                    </muDat>
                  </weightType>
                  <anhystereticParameter>
                    <a> 0.347647727062 </a>
                    <b> 4.749596097652 </b>
                    <c>0</c>
                    <d>0.0E1</d>
                    <onlyAnhyst>false</onlyAnhyst>
                    <forHalfRange>true</forHalfRange>
                    <anhystPartCountsTowardsOutputSat>true</anhystPartCountsTowardsOutputSat>
                  </anhystereticParameter>
                </weights>
                <weightsAdaptedToMayergoyzVectorModel>false</weightsAdaptedToMayergoyzVectorModel>
              </ScalarModel>
            </isotropic>
            <hystInversion>
              <InversionMethod> 
                <NewtonWithLinesearch>
                  <stopLinesearchAtLocalMin> true </stopLinesearchAtLocalMin>
                  <jacobiImplementation>
                    <ForwardBackwardDifferences></ForwardBackwardDifferences>
                  </jacobiImplementation>
                  <alphaLSMax>1.0E0</alphaLSMax>
                  <alphaLSMin>1.0E-3</alphaLSMin>
                  <jacobiResolution>1.0E-7</jacobiResolution>
                  <numberOfLinesearchSteps>50</numberOfLinesearchSteps>
                </NewtonWithLinesearch>
              </InversionMethod>
              <maxNumberOuterIterations>25</maxNumberOuterIterations>
			  <residualTolH value="1e-6" isRelative="no"/>
              <residualTolB value="1e-9" isRelative="no"/>
            </hystInversion>
            <clipOutputToSat>
              <noClipping></noClipping>
            </clipOutputToSat>
          </vectorPreisach_Mayergoyz>
        </magPolarization>
      </hystModel>
    </magnetic>
  </material>
  <!-- classical Scalar Preisach model -->
  <material name="FECO_SCALAR_EVERETTINVERSION">
    <magnetic>
      <electricConductivity>
        <linear>
          <isotropic>
            <real> 1.35e7 </real>
          </isotropic>
        </linear>
      </electricConductivity>
      <permeability>
        <linear>
          <isotropic>
            <real> 1.25663e-6 </real>
          </isotropic>
        </linear>
      </permeability>
      <hystModel>
	    <!-- documentation as for Mayergoyz 3 --><magPolarization>
          <scalarPreisach>
            <inputSat> 118807.897754412654 </inputSat>
			<outputSat> 1.776287668937 </outputSat>
			<dirPolarization>0 0 1</dirPolarization><weights>
              <dim_weights>200</dim_weights><weightType>
                <muDat>
                  <A> 2.654743006651 </A>
                  <h2> 0.089052884292 </h2>
                  <sigma2> 5.189442209595 </sigma2>
                  <eta> 1.620347107219 </eta>
                  <forHalfRange>true</forHalfRange>
                </muDat>
              </weightType>
              <anhystereticParameter>
                <a> 0.347647727062 </a>
                <b> 4.749596097652 </b>
                <c>0</c>
                <d>0.0E1</d>
                <onlyAnhyst>false</onlyAnhyst>
                <forHalfRange>true</forHalfRange>
                <anhystPartCountsTowardsOutputSat>true</anhystPartCountsTowardsOutputSat>
              </anhystereticParameter>
            </weights>
            <hystInversion>
			  <!-- SPECIAL: the scalar model allows for an efficient inversion based on a two-step search 
			  inside the Preisach-plane; details can be found in Phd-Thesis of F. Wolf "Generalisiertes Preisach Model ..." -->
              <InversionMethod>
                <EverettBasedInversion></EverettBasedInversion>
              </InversionMethod>
              <maxNumberOuterIterations>25</maxNumberOuterIterations>
			  <residualTolH value="1e-9" isRelative="no"/>
              <residualTolB value="1e-12" isRelative="no"/>
            </hystInversion>
          </scalarPreisach>
        </magPolarization>
      </hystModel>
    </magnetic>
  </material>
  
  <!-- vector Preisach model as proposed by A. Sutor et. al in its revised form;
  basic idea: extend scalar model by an additional vector-rotation operator -->
  <material name="FECO_Sutor_Revised">
    <magnetic>
      <electricConductivity>
        <linear>
          <isotropic>
            <real> 1.35e7 </real>
          </isotropic>
        </linear>
      </electricConductivity>
      <permeability>
        <linear>
          <isotropic>
            <real> 1.25663e-6 </real>
          </isotropic>
        </linear>
      </permeability>
      <hystModel>
        <magPolarization>
          <vectorPreisach_Sutor>
		  <!-- Big advantage of Sutor-vector model: weighting function (here muDat) can directly be taken from scalar model
		  without any further adaptationa (as it is required for Mayergoyz vector model -->
		  <inputSat> 118807.897754412654 </inputSat>
		  <outputSat> 1.776287668937 </outputSat><evalVersion>
              <Revised_Version_2015__list_implementation/>
			  </evalVersion>
			  <!-- the revised model requires two additional parameter; these can be determined by measuring a remanence drop
			i.e., bring material to saturation; rotate material by 90 degree; bring material to saturation again and measure 
			the decrease of the perpendicular component; can be done by a VVSM -->
			<rotResistance> 2.47582967393 </rotResistance>
			<angularDistance> 8.29786797402 </angularDistance>
			<!-- the vector model might obtain smaller values than outputSat at inputSat as the Preisach-plane might not be filled
			completely if rotResistance is smaller 1 in the revised model; by the following parameter one can perform a rescaling such
			that outputSat is reached at inputSat -->
			<enforceSatOutputAtSatInput>false</enforceSatOutputAtSatInput><weights>
			<!-- same weights as for scalar model; no adaption required -->
			<dim_weights>100</dim_weights>
			<weightType>
                <muDat>
                  <A> 2.654743006651 </A>
                  <h2> 0.089052884292 </h2>
                  <sigma2> 5.189442209595 </sigma2>
                  <eta> 1.620347107219 </eta>
                  <forHalfRange>true</forHalfRange>
                </muDat>
              </weightType>
              <anhystereticParameter>
                <a> 0.347647727062 </a>
                <b> 4.749596097652 </b>
                <c>0</c>
                <d>0.0E1</d>
                <onlyAnhyst>false</onlyAnhyst>
                <forHalfRange>true</forHalfRange>
                <anhystPartCountsTowardsOutputSat>true</anhystPartCountsTowardsOutputSat>
              </anhystereticParameter>
            </weights>
			<!-- currently not used at all (which explains the negative value; might be removed in future -->
            <debuggingParameter>
              <angularResolution>-1e-16</angularResolution>
            </debuggingParameter>  
            <hystInversion>
              <InversionMethod>
                <!-- utilize a local inversion based on a fix-point iteration -->
				<Fixpoint>
                  <!-- convergence factor > 1 improves the convergence range (i.e., for which starting values a convergence is achievable)
                    but decreases the convergence speed  -->
			<convergenceFactor>1.45E0</convergenceFactor>
			</Fixpoint>
			<!--
                  <NewtonWithLinesearch>
                  <stopLinesearchAtLocalMin> false </stopLinesearchAtLocalMin>
                  <jacobiImplementation>
                  <ForwardBackwardWithScaledDiagonal></ForwardBackwardWithScaledDiagonal>
                  </jacobiImplementation>
                  <alphaLSMax>1.0E0</alphaLSMax>
                  <alphaLSMin>1.0E-3</alphaLSMin>
                  <jacobiResolution>1.0E-12</jacobiResolution>
                  <numberOfLinesearchSteps>50</numberOfLinesearchSteps>
                  </NewtonWithLinesearch>
                -->
				</InversionMethod>
				<!-- locally, B = mu_0 H + J(H) is inverted; 
                residual with respect to H: res_H = H + ( J(H) - B )/mu_0
                residual with respect to B: res_B = B - mu_0 H - J(H)
                -> check res_H first (stricter criterion!); use res_B as failback -->
			  <residualTolH value="1e-8" isRelative="no"/>
              <residualTolB value="1e-11" isRelative="no"/>
              <!-- the used local inversion is H-based > very small steps > many steps required -->
              <maxNumberOuterIterations>500</maxNumberOuterIterations>
            </hystInversion>
          </vectorPreisach_Sutor>
        </magPolarization>
      </hystModel>
    </magnetic>
  </material>
  <!-- vector Preisach model proposed by Sutor et. al in its original form;
  takes more or less the same parameter as the revised model, only the values for
  rotResistance and angularDistance have to be adapted; angularDistance not used btw. -->
  <material name="FECO_Sutor_Classic">
    <magnetic>
      <electricConductivity>
        <linear>
          <isotropic>
            <real> 1.35e7 </real>
          </isotropic>
        </linear>
      </electricConductivity>
      <permeability>
        <linear>
          <isotropic>
            <real> 1.25663e-6 </real>
          </isotropic>
        </linear>
      </permeability>
      <hystModel>
        <magPolarization>
          <vectorPreisach_Sutor>
            <inputSat> 118807.897754412654 </inputSat>
			<outputSat> 1.776287668937 </outputSat>
			<evalVersion>
			<!-- both revised and classical version are available in a matrix and a list based implementation;
			the matrix based evaluation is way slower and should only be used for debugging and comparison; 
			list based implementation published by M. Nierla in "Stageless evaluation for a vector Preisach model based on ..." -->
			<Classical_Version_2012__list_implementation/>
			</evalVersion>
			<rotResistance> 2.47582967393 </rotResistance>
			<angularDistance>0</angularDistance>
			<enforceSatOutputAtSatInput>false</enforceSatOutputAtSatInput>
			<weights>
              <dim_weights>200</dim_weights>
			  <weightType>
                <muDat>
                  <A> 2.654743006651 </A>
                  <h2> 0.089052884292 </h2>
                  <sigma2> 5.189442209595 </sigma2>
                  <eta> 1.620347107219 </eta>
                  <forHalfRange>true</forHalfRange>
                </muDat>
              </weightType>
              <anhystereticParameter>
                <a> 0.347647727062 </a>
                <b> 4.749596097652 </b>
                <c>0</c>
                <d>0.0E1</d>
                <onlyAnhyst>false</onlyAnhyst>
                <forHalfRange>true</forHalfRange>
                <anhystPartCountsTowardsOutputSat>true</anhystPartCountsTowardsOutputSat>
              </anhystereticParameter>
            </weights>
            <hystInversion>
              <InversionMethod>
                <Fixpoint>
                  <convergenceFactor>1.125E0</convergenceFactor>
                </Fixpoint>
              </InversionMethod>
              <maxNumberOuterIterations>200</maxNumberOuterIterations>
			  <residualTolH value="1e-7" isRelative="no"/>
              <residualTolB value="1e-10" isRelative="no"/>
            </hystInversion>
          </vectorPreisach_Sutor>
        </magPolarization>
      </hystModel>
    </magnetic>
  </material>
</cfsMaterialDataBase>

<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <documentation>
        <title>Harmonic Static Coupling of Flat Plate</title>
        <authors>
            <author>kroppert</author>
        </authors>
        <date>2017-10-21</date>
        <keywords>
            <keyword>magneticEdge</keyword>
            <keyword>heatConduction</keyword>
            <keyword>harmonic</keyword>
            <keyword>static</keyword>
            <keyword>p-FEM-Legendre</keyword>
        </keywords>
        <references> Prechtl "Grundlagen der Elektrotechnik Band 2" Bsp. A26.9 </references>
        <isVerified>no</isVerified>
        <description> This is a simple cylindric flat aluminium plate with diameter D in a magnetic
            field with magnetic flux density normal to the plate. The alternating magnetic field has
            a amplitude of 1T and for a frequency of 50Hz, the induced current at the outer diameter
            is 53,4A/mm². The analytic solution for the diameter-dependent current is \hat{J}(r) =
            0.25 \omega \gamma \hat{B} r D .
            Source for HeatPDE is read from a harmonic magnetic simulation
            BC for HeatPDE is a heat-transport BC on the whole surface.
        </description>
    </documentation>
    <fileFormats>
        <input>
            <gmsh fileName="plateNew.msh"/>
        </input>
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>
    <domain geometryType="3d">
        <variableList>
            <var name="freq" value="500"/>
            <var name="B_amplitude" value="1"/>
        </variableList>
        <regionList>
            <region name="V_aluminium" material="alu"/>
        </regionList>
        <surfRegionList> </surfRegionList>
    </domain>

    <fePolynomialList>
        <Legendre id="Hcurl">
            <isoOrder>2</isoOrder>
        </Legendre>
        <Lagrange id="H1">
            <isoOrder serendipity="false">3</isoOrder>
        </Lagrange>
    </fePolynomialList>

    <sequenceStep index="1">
        <analysis>
            <harmonic>
                <numFreq>1</numFreq>
                <startFreq>100</startFreq>
                <stopFreq>100</stopFreq>
                <sampling>linear</sampling>
            </harmonic>
        </analysis>

        <pdeList>
            <magneticEdge>
                <regionList>
                    <region name="V_aluminium" polyId="Hcurl"/>
                </regionList>
                <bcsAndLoads>
                    <fluxDensity name="V_aluminium">
                        <comp dof="y" value="1"/>
                    </fluxDensity>
                </bcsAndLoads>
                
                <storeResults>
                    <elemResult type="magFluxDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magTotalCurrentDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotential" complexFormat="amplPhase">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magPotentialD1" complexFormat="amplPhase">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="magJouleLossPowerDensity">
                        <allRegions/>
                    </elemResult>
                    <elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>
                </storeResults>
            </magneticEdge>
        </pdeList>
    </sequenceStep>
            
            
    <sequenceStep index="2">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <heatConduction>
                <regionList>
                    <region name="V_aluminium" polyId="H1"/>
                </regionList>
                <bcsAndLoads>
                    <heatSourceDensity volumeRegion="V_aluminium" name="V_aluminium">   
                        <sequenceStep index="1">
                            <quantity name="magJouleLossPowerDensity" pdeName="magneticEdge"/>
                            <timeFreqMapping>
                                <constant step="1"/>
                            </timeFreqMapping>
                        </sequenceStep>
                    </heatSourceDensity>
                    <heatTransport name="S_aluminium" volumeRegion="V_aluminium" bulkTemperature="-10" heatTransferCoefficient="150"/>
                </bcsAndLoads>
                <storeResults>
                    <nodeResult type="heatTemperature">
                        <allRegions/>
                    </nodeResult>
                    <elemResult type="heatFluxDensity">
                        <allRegions/>
                    </elemResult>
                </storeResults>
            </heatConduction>
        </pdeList>       
    </sequenceStep>
</cfsSimulation>

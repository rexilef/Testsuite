finish
/clear
/filname,MagStrictMovingMag,1
/prep7
init


!				d1      wmag      d2    wprobe/2
!			|<----->|<--------->|<->|<----->|
!
!			_________________________________
!   		|								|	 ^
!   		|								|    |  d3
!   		|		_____________			|	 v
!   		|		|			|			|    ^
!   		|		|			|			|    |
!   		|		|			|			|	 |
!   		|		|			|			|    |
!   		|		|			|	________|    |
!   		0_ _ _ _|_ _ _ _ _ _| _ |_ _ _ _|    | movrange    ^ hprobe
!   		|		|			|	|_______|    |			   v	
!   		|		|			|			|    |
!   		|		|			|			|    |
!   		|		|			|			|    |
!   		|		|			|			|    |
!   		|		|___________|			|    v
!   		|								|    ^
!   		|								|    | d3
!   		|_______________________________|    v
!
!	movrange = possible movement range for magnet
!				actual height and positon of magnet has to be prescribed as boundary condition
!	0 = origin	
!

!!! Parameter !!!
d1 = 0.02
d2 = 0.01
d3 = 0.02
movrange = 0.08
wmag = 0.01
hprobe = 0.01
wprobe = 0.03
dx_inner = hprobe/2
dx_outer = dx_inner
tol = dx_inner/5

!!! Create geometry !!!
rectng,0,d1,-d3-movrange/2,movrange/2+d3

rectng,d1,d1+wmag,-d3-movrange/2,-movrange/2
rectng,d1,d1+wmag,-movrange/2,movrange/2
rectng,d1,d1+wmag,movrange/2,movrange/2+d3

rectng,d1+wmag,d1+wmag+d2,-d3-movrange/2,movrange/2+d3

rectng,d1+wmag+d2,d1+wmag+d2+dx_inner,-d3-movrange/2,-hprobe/2
rectng,d1+wmag+d2,d1+wmag+d2+dx_inner,-hprobe/2,hprobe/2
rectng,d1+wmag+d2,d1+wmag+d2+dx_inner,hprobe/2,movrange/2+d3

rectng,d1+wmag+d2+dx_inner,d1+wmag+d2+wprobe/2,-d3-movrange/2,-hprobe/2
rectng,d1+wmag+d2+dx_inner,d1+wmag+d2+wprobe/2,-hprobe/2,hprobe/2
rectng,d1+wmag+d2+dx_inner,d1+wmag+d2+wprobe/2,hprobe/2,movrange/2+d3

asel,all
aglue,all

!!! Create components !!!
asel,s,loc,x,d1,d1+wmag
asel,r,loc,y,-movrange/2,movrange/2
cm,magnet,area

asel,s,loc,x,d1+wmag+d2,d1+wmag+d2+wprobe/2
asel,r,loc,y,-hprobe/2,hprobe/2
cm,probe,area

asel,s,loc,x,d1+wmag+d2,d1+wmag+d2+dx_inner
asel,r,loc,y,-hprobe/2,hprobe/2
cm,observer,area

asel,all
cmsel,u,magnet
cmsel,u,probe
cm,air,area

lsel,s,loc,x,-tol,tol
cm,left,line

lsel,s,loc,x,d1+wmag+d2+wprobe/2-tol,d1+wmag+d2+wprobe/2+tol
lsel,r,loc,y,-hprobe/2,hprobe/2
cm,probe_right,line

lsel,s,loc,x,d1+wmag+d2+wprobe/2-tol,d1+wmag+d2+wprobe/2+tol
cmsel,u,probe_right
cm,right,line

lsel,s,loc,y,-d3-movrange/2-tol,-d3-movrange/2+tol
cm,bot,line

lsel,s,loc,y,d3+movrange/2-tol,d3+movrange/2+tol
cm,top,line

!!! Create mesh !!!
cmsel,s,probe
lsla,s
lesize,all,dx_inner

cmsel,s,left
cmsel,a,right
cmsel,a,top
cmsel,a,bot
lesize,all,dx_outer

lsel,all
lesize,all,dx_inner

setelems,'quadr',''
asel,all
amesh,all

setelems,'2d-line',''
cmsel,s,left
cmsel,a,probe_right
cmsel,a,right
cmsel,a,top
cmsel,a,bot
lmesh,all

!!! Write out mesh !!!
cmsel,s,magnet
esla,s
welems,'magnet'

cmsel,s,probe
esla,s
welems,'probe'

cmsel,s,observer
esla,s
wsavelem,'observer'

cmsel,s,air
esla,s
welems,'air'

cmsel,s,left
esll,s
welems,'left'

cmsel,s,probe_right
esll,s
welems,'probe_right'

cmsel,s,right
esll,s
welems,'right'

cmsel,s,top
esll,s
welems,'top'

cmsel,s,bot
esll,s
welems,'bot'

allsel
wnodes
mkhdf5

